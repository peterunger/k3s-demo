variable "hcloud_token" {}

terraform {
  backend "http" {
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

data "hcloud_location" "location" {
  name = "hel1"
}

resource "hcloud_ssh_key" "max_rosin" {
  name       = "Max Rosin"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbu3IzC8vwLVlp4+m+txwhXiXOEewvkCzBi6v1DJ6K5 max@gaia"
}

resource "hcloud_ssh_key" "niclas_mietz_1" {
  name       = "Niclas Mietz 1"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOIvRdcEUBH4wOVJC+YB+q6cTYTgiUWXWx2y/likaXoo Niclas Mietz (gitlab.com)"
}

resource "hcloud_ssh_key" "niclas_mietz_2" {
  name       = "Niclas Mietz 2"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM4mwv1Oud4DpFC77e4g62N2xEs+0yeEJAfqkVdLCPTf Niclas Mietz (gitlab.com)"
}

resource "hcloud_server" "server" {
  count       = 1
  name        = "server-${count.index}"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location    = data.hcloud_location.location.name
  ssh_keys    = [hcloud_ssh_key.max_rosin.id, hcloud_ssh_key.niclas_mietz_1.id, hcloud_ssh_key.niclas_mietz_2.id]
  labels = {
    "everyonecancontribute.com/server" = "true",
    "everyonecancontribute.com/agent"  = "false"
  }
  firewall_ids = [hcloud_firewall.base.id, hcloud_firewall.k3s-server.id]
}

resource "hcloud_server" "agent-cx21" {
  count       = 2
  name        = "agent-cx21-${count.index}"
  image       = "ubuntu-20.04"
  server_type = "cx21"
  location    = data.hcloud_location.location.name
  ssh_keys    = [hcloud_ssh_key.max_rosin.id, hcloud_ssh_key.niclas_mietz_1.id, hcloud_ssh_key.niclas_mietz_2.id]
  labels = {
    "everyonecancontribute.com/server" = "false",
    "everyonecancontribute.com/agent"  = "true"
  }
  firewall_ids = [hcloud_firewall.base.id]
}

resource "hcloud_network" "k3s" {
  name     = "k3s"
  ip_range = "10.29.0.0/16"
}

resource "hcloud_network_subnet" "k3s" {
  network_id   = hcloud_network.k3s.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "10.29.0.0/24"
}

resource "hcloud_server_network" "server" {
  count      = length(hcloud_server.server)
  server_id  = hcloud_server.server[count.index].id
  network_id = hcloud_network.k3s.id
}

resource "hcloud_server_network" "agent-cx21" {
  count      = length(hcloud_server.agent-cx21)
  server_id  = hcloud_server.agent-cx21[count.index].id
  network_id = hcloud_network.k3s.id
}

resource "hcloud_firewall" "base" {
  name = "base"
  rule {
    direction  = "in"
    protocol   = "icmp"
    source_ips = ["0.0.0.0/0", "::/0"]
  }
  rule {
    direction  = "in"
    protocol   = "tcp"
    port       = "22"
    source_ips = ["0.0.0.0/0", "::/0"]
  }
}

resource "hcloud_firewall" "k3s-server" {
  name = "k3s-server"
  rule {
    direction  = "in"
    protocol   = "tcp"
    port       = "6443"
    source_ips = ["0.0.0.0/0", "::/0"]
  }
}
