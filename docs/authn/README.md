# Authentication of Kubernetes

## Generate your Private Key

### Private Key 

- RSA
- Length 2048

```shell
openssl genrsa -out ${USERNAME}.key 2048
```

### Generate Signing request

```shell
openssl req -new -key ${USERNAME}.key -out ${USERNAME}.csr -subj "/O=${GROUP}/CN=${USERNAME}"
```

* Most Important are here the Subject for our singin Request
* Contains 
  * O ->  Organization Unit (Group in RBAC later)
  * CN -> CommonName (Username in RBAC later)


## Generate your Signing Request

```shell
cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  # This has to match the id that you will use
  name: ${USERNAME}
spec:
  groups:
  # This means we want to add this csr to all of the authenticated users
  - system:authenticated
  request: $(cat ${USERNAME}.csr | base64 | tr -d "\n")
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
EOF
```

## Approve Sign of the CSR

`kubectl certificate approve ${USERNAME}`

## Fetch Signed Certificate

`kubectl get csr/${USERNAME} -o jsonpath="{.status.certificate}" | base64 -d > ${USERNAME}.crt`


## Connect to Kubernetes

`export KUBECONFIG=$HOME/.kube/k3s-demo`

`kubectl config set-credentials ${USERNAME} --client-key=${USERNAME}.key --client-certificate=${USERNAME}.crt --embed-certs=true`


`kubectl config set-context ${USERNAME} --cluster=k3s-demo --user=${USERNAME}`

`kubectl config use-context ${USERNAME}`



## Authorization


## Cluster Role




### Cluster Role Binding

```shell
cat <<EOF | kubectl apply -f -
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ${GROUP}-view-binding
subjects:
- kind: Group
  # This value is the one that k8s uses to define group membership
  # Must be the same in the openssl subject
  name: ${GROUP}
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: view
  apiGroup: rbac.authorization.k8s.io
EOF
```

### Role Binding

```shell
cat <<EOF | kubectl apply -f -
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ${GROUP}-admin-binding
  namespace: ${GROUP}
subjects:
- kind: Group
  # This value is the one that k8s uses to define group membership
  # Must be the same in the openssl subject
  name: ${GROUP}
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: admin
  apiGroup: rbac.authorization.k8s.io
EOF
```

