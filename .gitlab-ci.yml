stages:
  - ansible-prep
  - test
  - terraform-diff
  - terraform
  - ansible-diff
  - ansible
  - kubeconfig

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"

variables:
  KUBECONFIG: ${CI_PROJECT_DIR}/.kubeconfig

.ansible:
  image: $CI_REGISTRY_IMAGE:latest
  before_script:
    - chmod go-rwx ${SSH_KEY}
    - cd ansible
  variables:
    HCLOUD_TOKEN: ${hcloud_token}
    ANSIBLE_CONFIG: ./ansible.cfg

.terraform:
  image: registry.gitlab.com/gitlab-org/terraform-images/releases/0.14:v0.9.0
  variables:
    TF_VAR_hcloud_token: ${hcloud_token}
    # alternative: the provider can read the token directly from HCLOUD_TOKEN
    TF_ROOT: ${CI_PROJECT_DIR}/terraform
    TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/default
  cache:
    key: terraform-default
    paths:
      - terraform/.terraform
  before_script:
    - cd terraform

TF Validate:
  stage: test
  extends: .terraform
  script:
    - gitlab-terraform init
    - gitlab-terraform validate

TF Plan:
  stage: terraform-diff
  extends: .terraform
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  artifacts:
    name: plan
    paths:
      - terraform/plan.cache
    reports:
      terraform: terraform/plan.json
    expire_in: 1 week

TF Apply:
  stage: terraform
  extends: .terraform
  script:
    - gitlab-terraform apply

# Ansible Baseimage:
#   # Use the official docker image.
#   image: docker:latest
#   stage: ansible-prep
#   services:
#     - docker:dind
#   before_script:
#     - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
#     - cd ansible
#   # Default branch leaves tag empty (= latest tag)
#   # All other branches are tagged with the escaped branch name (commit ref slug)
#   script:
#     - |
#       job_branch_log_msg="Running job on branch: $CI_COMMIT_BRANCH"
#       if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
#         tag=""
#         echo "$job_branch_log_msg (default branch, latest tag)"
#       else
#         tag=":$CI_COMMIT_REF_SLUG"
#         echo "$job_branch_log_msg (tagging with $tag)"
#       fi
#     - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" .
#     - docker push "$CI_REGISTRY_IMAGE${tag}"
#   rules:
#     - if: $CI_COMMIT_BRANCH
#       exists:
#         - ansible/Dockerfile
#       changes:
#         - .gitlab-ci.yml
#         - ansible/Dockerfile
#         - ansible/requirements.txt
#         - ansible/requirements.yml

Ansible Diff:
  extends: .ansible
  stage: ansible-diff
  script:
    - ansible-playbook --private-key="${SSH_KEY}" --extra-vars="ansible_ssh_private_key_file=${SSH_KEY}" main.yml --skip-tags kubeconfig --check

Ansible Apply:
  extends: .ansible
  stage: ansible
  script:
    - ansible-playbook --private-key="${SSH_KEY}" --extra-vars="ansible_ssh_private_key_file=${SSH_KEY}" main.yml --skip-tags kubeconfig

Kubeconfig:
  extends: .ansible
  stage: kubeconfig
  script:
    - ansible-playbook --private-key="${SSH_KEY}" --extra-vars="ansible_ssh_private_key_file=${SSH_KEY}" main.yml --tags kubeconfig --extra-vars="kubeconfig_local_path=${KUBECONFIG}"
  artifacts:
    name: kubeconfig
    paths:
      - ${KUBECONFIG}
    expire_in: 2 days
