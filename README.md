# k3s on hcloud

- [#everyonecancontribute](https://everyonecancontribute.com)
- [Repository](https://gitlab.com/ekeih/k3s-demo)
- [Twitter](https://twitter.com/ekeih)
- [Blog](https://fotoallerlei.com)

## Rough Agenda

- Terraform
- Ansible
- k3s & Kubernetes
- hcloud: cloud controller manager & CSI driver
- helmfile & helm
- Optional
  - Everything in GitLab CI
  - sealed-secrets
  - hcloud: autoscaler
  - Deploy monitoring

## Terraform

- [Terraform](https://www.terraform.io/)
- [hcloud provider](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs)
- [State Backend in GitLab](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html)

```
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/23944451/terraform/state/default" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/23944451/terraform/state/default/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/23944451/terraform/state/default/lock" \
    -backend-config="username=ekeih" \
    -backend-config="password=${GITLAB_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

- [GitLab Module Registry](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/18834)
- [MR Widget](https://gitlab.com/groups/gitlab-org/-/epics/3441)
- [Protected State](https://gitlab.com/gitlab-org/gitlab/-/issues/227108)
- [GitLab Cluster Creation with Terraform](https://gitlab.com/gitlab-org/gitlab/-/issues/220201)
- [Terraform Workspaces](https://www.terraform.io/docs/language/state/workspaces.html)

## Ansible

- [Ansible](https://docs.ansible.com/)
- [Dynamic Inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_dynamic_inventory.html)
- [hcloud collection](https://docs.ansible.com/ansible/latest/collections/hetzner/hcloud/hcloud_inventory.html)
- [DebOps](https://docs.debops.org)
- [Terraform Inventory](https://github.com/adammck/terraform-inventory)

## Hetzner / hcloud

- [Cloud Controller Manager](https://github.com/hetznercloud/hcloud-cloud-controller-manager)
- [CSI Drive](https://github.com/hetznercloud/csi-driver)

### Cloud Controller Manager

```
kubectl -n kube-system create secret generic hcloud --from-literal=token=${HCLOUD_TOKEN} --from-literal=network=k3s
```

- [Network Support](https://github.com/hetznercloud/hcloud-cloud-controller-manager/blob/master/docs/deploy_with_networks.md)
